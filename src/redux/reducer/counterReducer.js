// /it is higher order function///////
import { AddCounter, SubCounter } from "../actions/actionType";
export default function counterReducer(initialState = 0, action) {
    debugger
    switch (action.type) {
        case AddCounter:
            return initialState + 1;
        case SubCounter:
            return initialState - 1;
        default:
            return initialState;
    }

}